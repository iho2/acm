// Generate PDF (https://www.codexworld.com/convert-html-to-pdf-using-javascript-jspdf/)
function downloadPDF(){
  // Get the contents of the textarea
  var textAreaContent = document.getElementById("textbox").value;

  //text = text.replace(/∀/g, "&#8704;");
  textAreaContent = textAreaContent.replace(/&/g, "&amp;");

  // Create a new jsPDF object
  var doc = new jsPDF();

  // Add the text to the PDF document
  doc.text(textAreaContent, 10, 10);

  // Save the PDF document
  doc.save("myDocument.pdf");
}

// Download Text File (https://foolishdeveloper.com/save-textarea-text-to-a-file-using-javascript/)
function downloadFile(content) {
  const element = document.createElement('a');
  // store binary data
  const blob = new Blob([content], { type: 'plain/text' });
  const fileUrl = URL.createObjectURL(blob);

  element.setAttribute('href', fileUrl);
  element.setAttribute('download', 'myDoc');

  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();

  document.body.removeChild(element);
};

window.onload = () => {
  document.getElementById('download').
  addEventListener('click', e => {
    const content = document.getElementById('textbox').value;
    if (content) {
      downloadFile(content);
    }
  });
};

// Download the image w/ special charcters in pdf
function downloadimg(){
  const doc = new jsPDF();
  //var textAreaContent = document.getElementById("textbox").value;


  html2canvas(document.querySelector("#textbox")).then((canvas) => {
    const imageData = canvas.toDataURL("image/jpeg");

    doc.addImage(imageData, "JPEG", 0, 0, doc.internal.pageSize.width, 0);
    doc.save("document.pdf");
  });
}