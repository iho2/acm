let json_data = JSON.parse(localStorage.getItem('char_data'));
let shortcuts = JSON.parse(localStorage.getItem('shortcuts'));

let info = document.getElementById('instructions');
let button_div = document.getElementById('buttons');
let data_div = document.getElementById('data');

// hold data for new char to be added
let new_c = document.createElement('div');
// hold the data for all the current chars
let curr_c = document.createElement('div');
curr_c.className = 'container';

// mode can be "normal", "add", or "delete"
let mode = "normal";

// variable for storing table entry's old shortcut
// useful for deleting shortcuts
let entry_to_shortcut = {}; // key = entry # (get this from table row id), val = shortcut

//set up base instructions
function setInfo(text) {
    info.textContent = text;
}

// EMILY HO HO LOOK OVER HEREEEEEE CAN YOU SEE THIS VERY LONG COMMENT LINE? NOW DO STUFF :)
// update data
function updateData() {
    console.log("inside update data");
    console.log(json_data);
    console.log(shortcuts);
    localStorage.setItem('char_data', JSON.stringify(json_data));
    localStorage.setItem('shortcuts', JSON.stringify(shortcuts));
}

function updateNode(bool, node) {
    let children = node.childNodes;
    children.forEach(item => {
        console.log('updating: ' + item.textContent);
        item.draggable = bool;
    });
}

// updates dragged and dropped sequence into local storage
function logSequence(){
    let items = document.querySelectorAll('.container .box');
    let seq = [];
    items.forEach(function(item) {
        seq.push(item.textContent);
    });
    
    let json_seq = seq;

    json_data.basic.forEach(char => {
        let index = json_seq.indexOf(char.symbol);
        json_seq[index] = char;
    });

    json_data = {"basic": json_seq};
    updateData();
}

// set up main action buttons
function setActions() {
    let a_elem = document.createElement('a');
    a_elem.href = "./index.html";
    let return_b = document.createElement('button');
    return_b.textContent = 'RETURN';

    return_b.addEventListener('click', () => {
        // return to main page and save content
        logSequence(); // log sequence contains updateData();
        //updateData();
        //window.location("https://google.com");
        //window.location('./index.html'); // go back to main page might not work
        
    });
    a_elem.appendChild(return_b);
    a_elem.href = "./index.html";
    button_div.appendChild(a_elem);
    
    let add_b = document.createElement('button');
    add_b.addEventListener('click', () => {
        // add a new character
        mode = "add";
        logSequence(); // just in case they swapped things before adding more
        changeMode();
    });
    add_b.textContent = 'ADD';
    button_div.appendChild(add_b);

    let delete_b = document.createElement('button');
    delete_b.addEventListener('click', () => {
        // delete selected character
        mode = "delete";
        logSequence(); // just in case they swapped things before deletion mode`
        changeMode();
    });
    delete_b.textContent = 'DELETE';
    button_div.appendChild(delete_b);
}

// drag and drop setup from https://web.dev/drag-and-drop/
document.addEventListener('DOMContentLoaded', (event) => {

    function handleDragStart(e) {
        this.style.opacity = '0.4';

        dragSrcEl = this;

        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', this.innerHTML);
    }

    function handleDragEnd(e) {
        this.style.opacity = '1';

        items.forEach(function (item) {
            item.classList.remove('over');
        });
    }

    function handleDragOver(e) {
        e.preventDefault();
        return false;
    }

    function handleDragEnter(e) {
        this.classList.add('over');
    }

    function handleDragLeave(e) {
        this.classList.remove('over');
    }
  
    function handleDrop(e) {
        e.stopPropagation(); // stops the browser from redirecting.
        
        if (dragSrcEl !== this) {
            dragSrcEl.innerHTML = this.innerHTML;
            this.innerHTML = e.dataTransfer.getData('text/html');
        }
        
        return false;
    }

    let items = document.querySelectorAll('.container .box');
    items.forEach(function(item) {
        item.addEventListener('dragstart', handleDragStart);
        item.addEventListener('dragover', handleDragOver);
        item.addEventListener('dragenter', handleDragEnter);
        item.addEventListener('dragleave', handleDragLeave);
        item.addEventListener('dragend', handleDragEnd);
        item.addEventListener('drop', handleDrop);
    });
});

// stores all the selected buttons
let selected_buttons = [];
let count = 0;
// create the data
function makeData() {
    // create the current data buttons
    console.log("json data");
    console.log(json_data);
    json_data.basic.forEach(char => {
        count++;
        let button = document.createElement('div');
        button.id = "button" + count;
        button.draggable = true;
        button.className = 'box';
        button.textContent = char.symbol;
        button.addEventListener("click",(event) => {
            //this event is deselecting the button
            if (mode == "delete" && button.style.color == "red") {
                // deletes button.id from selected_buttons
                selected_buttons = selected_buttons.filter((letter) => {
                    return letter !== button.id;
                });
                button.style.color="black";
            }
            // this event is selecting the button to delete
            else if (mode == "delete"){
                selected_buttons.push(button.id);
                button.style.color="red";
            }
            console.log("button.id is " + button.id);
            console.log(selected_buttons);
        });

        curr_c.appendChild(button);
    });
}

// true when delete is confirm, false all other times
// event listener for when confirm delete button is clicked
document.getElementById('confirm-button-delete').addEventListener('click', (event) => {
    document.getElementById("confirm-modal-delete").style.display="none";
    
    if (curr_c.firstChild.draggable == false) {
        updateNode(false,curr_c);
    }

    if (data_div.hasChildNodes()) {
        data_div.removeChild(data_div.firstChild);
    }

    data_div.appendChild(curr_c);

    // goes through all the buttons in selected_buttons (stores the id of the children) and delete each element
    selected_buttons.forEach((elemID) => {
        let symbol = document.getElementById(elemID).innerHTML;
        console.log("symbol is " + symbol);
        document.getElementById(elemID).remove();
        let shortcut = "";
        // goes through the whole array containing our data. Once we find the right element, get its shortcut key
        json_data.basic.forEach((chara_obj) => {
            if (chara_obj.symbol == symbol) {
                shortcut = chara_obj.shortcut;
            }
        });
        // delete shortcut if it's not ""
        if (shortcut != "") {
            delete shortcuts[shortcut];
            console.log("after deleting " + shortcut);
            console.log(shortcuts[shortcut]);
        }
        // delete from json_obj
        json_data.basic = json_data.basic.filter((chara_obj) => {return chara_obj.symbol !== symbol});

        console.log(json_data);
        console.log(shortcuts);
        
        
    });
    selected_buttons = [];
    confirm_status = false;

    // figure out a way to track/show what buttons are selected
    // click confirm (make a confirm button) then: 
        // delete said buttons from json_data -> then push change to local storage
        // return to normal mode
    mode = "normal";

});

let curr_shortcuts = JSON.parse(JSON.stringify(shortcuts)); // makes a copy of the shortcuts array
let ind = 0;
// tracking the modes
function changeMode(){
    console.log("change mode called and mode is " + mode);
    if (mode == 'normal') {
        document.getElementById('confirm-modal-delete').style.display = "none";
        // set up for main page
        setInfo('Drag and Drop to reorder your character list.\n\
        You can also add new characters or delete old characters :)');

        if (curr_c.firstChild.draggable != true) {
            updateNode(true,curr_c);
        }

        if (data_div.hasChildNodes()) {
            data_div.removeChild(data_div.firstChild);
        }

        data_div.appendChild(curr_c);



    } else if (mode == 'delete') {
        // deleting buttons
        setInfo('Select the buttons you want to delete!');
        // makes the confirm button appear
        // the actual deletion happens after the confirm button is clicked (check the code inside its event listener)
        document.getElementById("confirm-modal-delete").style.display = "flex";

    

        
    } else if (mode == 'add') {
        document.getElementById('confirm-modal-delete').style.display="none";
        // adding a new button
        setInfo('Copy and Paste a new charater you want!\n\
            Give it a name and a shortcut!'); 
        
        document.getElementById("confirm-modal-add").style.display = "flex";

        document.getElementById("confirm-modal-add").addEventListener("click", () => {
            let new_chars = [];
            let items = document.querySelectorAll('.add_table .add_item');
            items.forEach(function(item) {  // these are the tr tags
                let children = item.childNodes;
                let c_data = [];
                children.forEach(child => { // these are the td tags
                    console.log(child.firstChild.value);
                    c_data.push(child.firstChild.value);
                });
                new_chars.push({"name": c_data[1], "symbol": c_data[0], "shortcut": c_data[2]});
                shortcuts[c_data[2]] = c_data[0]; // adds to shortcut
            });
            console.log(new_chars);

            console.log(json_data);
            json_data['basic'] = json_data['basic'].concat(new_chars);
            console.log(json_data['basic']);
            console.log("shortcuts");
            console.log(shortcuts);
            

            updateData();

            mode = 'normal';
            changeMode();

            entry_to_shortcut = {}; // reset to empty after everything is added into local storage
            // reloads the page so it updates
            location.reload();
        });

        if (data_div.hasChildNodes()) {
            data_div.removeChild(data_div.firstChild);
        }

        data_div.appendChild(new_c);

        while (new_c.firstChild) {
            new_c.removeChild(new_c.firstChild);
        }

        let table = document.createElement('table');
        new_c.appendChild(table);
        table.className = "add_table";

        let titles = document.createElement('tr');
        headers = ['Char', 'Name', 'Shortcut'];
        for (i = 0; i < headers.length; i++) {
            let title_name = document.createElement('th');
            title_name.textContent = headers[i];
            titles.appendChild(title_name);
        }
        table.appendChild(titles);

        function addRow() {
            ind++;
            let char_id, name_id;
            // console.log('add row!');
            let container = document.createElement('tr');
            container.className = "add_item";
            for (i = 0; i < headers.length; i++) {
                let column = document.createElement('td');    
                let inputbox = document.createElement('input');
                if (i == 0) {
                    char_id = "char" + ind;
                    inputbox.id = char_id;
                }
                if (i == 1) {
                    name_id = "name" + ind;
                    inputbox.id = name_id;
                }
                if (i != 1) inputbox.maxLength = 1;
                // creating the shortcut box, need to make sure it's a valid/unused shortcut
                if (i == 2) {
                    
                    inputbox.id = "input" + ind;
                    shortcut_id = inputbox.id;
                    inputbox.addEventListener("input", (event) => {
                        // gets previous shortcut for the specific symbol and delete it
                        let old_shortcut = entry_to_shortcut[inputbox.id];
                        // deletes previous shortcut
                        delete curr_shortcuts[old_shortcut];
                        //chara.shortcut = (event.data == null) ? "" : event.data;
                        console.log("char id's value is " + document.getElementById(char_id).value);
                        if (document.getElementById(char_id).value == "" || document.getElementById(name_id).value == ""){ // can't type a shortcut until the character and name field is filled
                            event.preventDefault();
                            inputbox.value = "";
                        }
                        else if (event.data != null)  {

                            if ((event.data in curr_shortcuts) == true ) {//|| (event.data in //EMILY HOHO YOU MESSED UP WHAT DO YOU WANT EMILY HOHOHOHOHOHOHOHOHO)) { // duplicate shortcut, INVALID
                                console.log(event.data + " is already used!!! Choose a new one");
                                document.getElementById(inputbox.id).value = "";
                                //chara.shortcut="";
                                console.log("inputBox.id is " + inputbox.id);
                                console.log(entry_to_shortcut);
                                entry_to_shortcut[inputbox.id] = "";
                            }
                            else {
                                //shortcuts[event.data] = chara.symbol;
                                curr_shortcuts[event.data] = document.getElementById(char_id).value;
                                entry_to_shortcut[inputbox.id] = event.data;
                            }
                        
                        }
                        else {
                            entry_to_shortcut[inputbox.id] = "";
                        }
                        console.log("entry_to_shortcut");
                        console.log(entry_to_shortcut);
                        console.log(curr_shortcuts);
                        //updateData();
 


                    });
                }
                column.appendChild(inputbox);
                container.appendChild(column);
            }
            table.appendChild(container);
        }
        
        addRow(); // make the first row
        
        let change_row = document.createElement('div');
        new_c.appendChild(change_row);

        let add_row_b = document.createElement('button');
        change_row.appendChild(add_row_b);
        add_row_b.textContent = 'Add Another Row';
        add_row_b.addEventListener("click", addRow);

        let remove_row_b = document.createElement('button');
        change_row.appendChild(remove_row_b);
        remove_row_b.textContent = 'Delete Last Row';
        remove_row_b.addEventListener("click", () => {
            table.removeChild(table.lastChild);
        })

        // create a table with names and 3 input boxes, symbol, name, shortcut
        // user could copy paste into any of the boxes and set shortcut
        // 2 buttons to be made:
            // confirm -> return to normal mode and add to json_data, push changes
            // add another -> add another row to table that allows for more new data

        mode = "normal";

    }
}

// initialize the page
function initPage() {
    setActions();
    makeData();
    changeMode('normal');
}

initPage();