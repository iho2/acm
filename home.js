let data;
let shortcuts;



// initializes or loads char_data into app
if (!localStorage.getItem('char_data')) { //first time visiting page
    console.log('no storage :(');
    //gets the json data into the variable json_data
    let json_data;
    $.ajax({
        url: "characters.json",
        async: false,
        dataType: 'json',
        success: function(fetched_data) {
            json_data = fetched_data;
        }
    });
    console.log("char_data doesn't exist, fetch from json");
    console.log("char_data is ");
    console.log(json_data);
    
    data = json_data;
} else {
    data = JSON.parse(localStorage.getItem('char_data'));
    console.log("char_data already exist in local storage, its value is ");
    console.log(data);
}


// initializes or loads shortcuts into app
if (localStorage.getItem('shortcuts') == null) { //shortcuts doesn't exist in localStorage yet
    console.log('no storage :(');
    shortcuts = {}; // initialize empty shortcut object;
    updateData();
} else {
    console.log("shortcuts already exist. Its value (before parsing) is ");
    console.log(localStorage.getItem('shortcuts'));
    shortcuts = JSON.parse(localStorage.getItem('shortcuts'));
    updateData();
}

// updates data to local storage
function updateData() {
    localStorage.setItem('char_data', JSON.stringify(data)); 
    localStorage.setItem('shortcuts', JSON.stringify(shortcuts));
}

// variable for storing each table entry's old shortcut
// useful for deleting shortcuts
let entry_to_shortcut = {}; // key = entry # (get this from table row's ID), val = shortcut

// displays all the characters on the webpage
let char_table = document.getElementById('char_table');
let text_box = document.getElementById('textbox');
let count = 0;

data.basic.forEach(chara => {
    count++;
    let formatted_chara = document.createElement('tr');
    formatted_chara.id = "entry" + count;

    let char = document.createElement('td');
    char.textContent = chara.symbol;
    char.addEventListener("click", () => {
        // no emily ho ho here >:(
        // console.log(chara.symbol + " was clicked!");
        text_box.value += chara.symbol;
        // console.log("textbox content: " + text_box.value);
    });

    let name = document.createElement('td');
    name.textContent = chara.name;

    let shortcut = document.createElement('td');
    let inputBox = document.createElement('input');
    inputBox.id = "in" + count;
    entry_to_shortcut[inputBox.id] = chara.shortcut;
    inputBox.defaultValue = chara.shortcut;
    inputBox.maxLength = 1;
    if (count%2 == 1) { 
        inputBox.style.backgroundColor = "rgb(146,146,146)";
    }
    inputBox.addEventListener("input", (event) => {
        /**
         * pseudo:
         * 1. if there was an old shortcut, delete that from shortcuts (and update local storage)
         * 2. get the new shortcut that is inputted. update chara.shortcut, local storage, shortcuts, and entry_to_shortcut
         * 3. extra: input validation (for later)
         */
        // gets previous shortcut for the specific symbol and delete it
        let old_shortcut = entry_to_shortcut[inputBox.id];
        // deletes previous shortcut
        delete shortcuts[old_shortcut];
        chara.shortcut = (event.data == null) ? "" : event.data;
        if (event.data != null)  {
            
            if ((event.data in shortcuts) == true) { // duplicate shortcut, INVALID
                alert(event.data + " is already used!!! Choose a new one");
                document.getElementById(inputBox.id).value = "";
                chara.shortcut="";
                console.log("inputBox.id is " + inputBox.id);
                console.log(entry_to_shortcut);
                entry_to_shortcut[inputBox.id] = "";
            }
            else {
                shortcuts[event.data] = chara.symbol;
                entry_to_shortcut[inputBox.id] = event.data;
            }

        }
        else {
            entry_to_shortcut[inputBox.id] = "";
        }
        updateData();
        
    });

    shortcut.appendChild(inputBox);
    //shortcut.textContent = chara.shortcut;
        
    formatted_chara.appendChild(char);
    formatted_chara.appendChild(name);
    formatted_chara.appendChild(shortcut);

    char_table.appendChild(formatted_chara);
});





// switches between normal mode vs.shortcut mode
// Ctrl + G --> switches into and out of shortcut mode
// Ctrl + M --> single use of shortcut mode
let mode = "default";
// detects whenever 
document.addEventListener('keyup', (event) => {
    let name = event.key;
    let code = event.code;
    // ctrl + Q is clicked, switch to shortcut mode or default mode
    if ((name == "q" || name == "Q") && event.ctrlKey) {
        console.log("switching mode");
        if (mode == "shortcut") mode = "default";
        else mode = "shortcut";
    }
    // ctrl + M is clicked, switch to single shortcut mode
    else if ((name == "m" || name == "M") && event.ctrlKey && mode != "shortcut") {
        mode = "single";
    }
    // rest of the keys: depending on whether the cursor is in the textbook and the mode, output different things
    else {
        ;
    }
    console.log("mode is " + mode);
    console.log(shortcuts);
    console.log(entry_to_shortcut);
});

// event listener for when keys are pressed within the textArea
document.getElementById("textbox").addEventListener("keydown", (event) => {
    // ascii for 0-9 48-57
    // ascii for a-z 65-90
    // use keycode to get the ascii value of character
    if ((mode == "shortcut" || mode == "single") && event.ctrlKey == false && ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90))) {
        if (shortcuts[event.key] != null) document.getElementById("textbox").value += shortcuts[event.key];
        else {
            console.log("Invalid shortcut " + event.key);
        }
        event.preventDefault(); // prevent the character from being typed into the textarea if we're in shortcut mode
        if (mode == "single") mode = "default"; // in single shortcut mode, exit into default mode
    }
});

function f1() {
    //function to make the text bold using DOM method
    document.getElementById("textbox").style.fontWeight = "bold";
}
  
function f2() {
    //function to make the text italic using DOM method
    document.getElementById("textbox").style.fontStyle = "italic";
}
  
function f3() {
    //function to make the text alignment left using DOM method
    document.getElementById("textbox").style.textAlign = "left";
}
  
function f4() {
    //function to make the text alignment center using DOM method
    document.getElementById("textbox").style.textAlign = "center";
}
  
function f5() {
    //function to make the text alignment right using DOM method
    document.getElementById("textbox").style.textAlign = "right";
}
  
function f6() {
    //function to make the text in Uppercase using DOM method
    document.getElementById("textbox").style.textTransform = "uppercase";
}
  
function f7() {
    //function to make the text in Lowercase using DOM method
    document.getElementById("textbox").style.textTransform = "lowercase";
}
  
function f8() {
    //function to make the text capitalize using DOM method
    document.getElementById("textbox").style.textTransform = "capitalize";
}
  
function f9() {
    //function to make the text back to normal by removing all the methods applied 
    //using DOM method
    document.getElementById("textbox").style.fontWeight = "normal";
    document.getElementById("textbox").style.textAlign = "left";
    document.getElementById("textbox").style.fontStyle = "normal";
    document.getElementById("textbox").style.textTransform = "capitalize";
    document.getElementById("textbox").value = " ";
}

function f10() {
    //function to make the text in normal using DOM method
    document.getElementById("textbox").style.fontWeight = "normal";
}

// dark mode
function dark() {
    let element = document.body;
    let textarea = document.getElementById("textbox");
    //let btn = document.getElementsByClassName("btn");
    element.classList.toggle("dark-mode");
    textarea.classList.toggle("dark-textarea");
    //btn.classList.toggle("dark-button");
  }

  // clears local storage
 function clear_storage() {
    localStorage.clear();
    location.reload();
  }